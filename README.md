# Shadowsocks Compose
Быстрый гайд для нюфань. Носки за клаудой по определенному path. Nginx по корню возвращает [echoip](https://github.com/mpolden/echoip)  
Только Debian-based, остальное сам.  
Метод шифрования по умолчанию *chacha20-ietf-poly1305*
* Что-то не работает - пиши в тред
* Хочу помочь - открывай мерж реквест
* Гайд хуйня - да

## Требования

* Зареганый домен ([freenom](https://www.freenom.com/ru/index.html) или [porkbun](https://porkbun.com/) дешевые .top .xyz) и назначенные неймсервера на cloudflare
* VPS с debian/ubuntu (512+mb RAM)
* Уметь гуглить свои проблемы

## Настройка

```
sudo -s
apt update
apt install -y docker docker.io docker-compose nano git curl
#Enable BBR
bash <(curl -sL https://gist.githubusercontent.com/duzun/1503ebefbb208b9072ef85b7190364a4/raw/724ed096db887ef7ba80d6685779c433f8f69313/arch_enable_bbr.sh)
git clone https://gitlab.com/Relers/shadowsocks-compose.git
cd shadowsocks-compose
```
**Далее:**  
Зайти в директорию `screenshots`, проделать действия 1-4, 5 Origin Certificate вставить в `mysite.cert`, Private Key в `mysite.key`, они будут монтироваться в nginx контейнер.  
Далее надо отредактировать файл .env, думаю тут всё понятно, path это скрытый путь по которому доступ к shadowsocks.
```
nano .env

PASSWORD=fuckrkn
DOMAIN=perdolik.tk
SS_PATH=/v2ray
DNS=1.1.1.1

ctrl+x
Y
```
Старт-стоп контейнеров
```
# Старт
docker-compose up -d
# Стоп
docker-compose down
# Показать запущенные контейнеры
docker ps
# Показать логи контейнера
docker logs -f ss-v2ray
```



## Настройка на мобиле
* Скачать из gplay [shadowsocks](https://play.google.com/store/apps/details?id=com.github.shadowsocks&hl=ru&gl=US) и [v2ray-plugin](https://play.google.com/store/apps/details?id=com.github.shadowsocks.plugin.v2ray)
* Смотреть скриншоты 6-8, не забыть заменить свой домен и path.


## Результат

## To Do
- [X] Написать Readme v1
- [X] Разобраться с б**дскими энвами у nginx
- [X] Репа переехала в [gh](https://github.com/Relers/shadowsocks-compose)
